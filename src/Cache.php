<?php

namespace Lliure;


use ArrayAccess;

/**
 *
 */
class Cache
{

	/** @var ArrayAccess|array */
	protected $storage;
	
	/** @var string */
	protected string $keySerializer;
	
	/** @var string Sha512 */
	protected string $keyHash;

	/**
	 * @param array|ArrayAccess $storage
	 * @param array             $key
	 */
	protected function __construct(&$storage, array $key){
		$this->storage =& $storage;
        $this->keySerializer = serialize($key);
		$this->keyHash = hash('sha512', $this->keySerializer);
	}

	protected function exists(): bool{
		if($this->storage instanceof ArrayAccess){
			return $this->storage->offsetExists($this->keyHash);
		}
        return isset($this->storage[$this->keyHash]);
    }

	protected function &set(callable $handler, ...$params){
		try{
			$this->storage[$this->keyHash] = $handler(...$params);
		}catch(\Exception $exception){
			$this->storage[$this->keyHash] = $exception;
		}
		return $this->get();
	}

	protected function &get(){
		if($this->storage[$this->keyHash] instanceof \Throwable){
			throw $this->storage[$this->keyHash];
		}
		return $this->storage[$this->keyHash];
	}
	
	protected function del(){
		if($this->storage instanceof ArrayAccess){
            $this->storage->offsetUnset($this->keyHash);
        }else{
            unset($this->storage[$this->keyHash]);
        }
	}
	
	/**
	 * @param array|ArrayAccess $storage
	 * @param array             $key
	 * @param callable          $callable
	 * @param                   ...$params
	 * @return mixed
	 * @throws \Throwable
	 */
	public static function &exec(&$storage, array $key, callable $callable, ...$params){
		$self = new self($storage, $key);
		if($self->exists()){
            return $self->get();
        }
		return $self->set($callable, ...$params);
	}

	/**
	 * @param array|ArrayAccess $storage
	 * @param array             $key
	 */
	public static function remove(&$storage, array $key){
		(new self($storage, $key))->del();
	}

	/**
	 * @param array|ArrayAccess $storage
	 * @param array             $key
	 * @return bool
	 */
	public static function isset(&$storage, array $key): bool{
		$self = new self($storage, $key);
		return $self->exists();
	}

}